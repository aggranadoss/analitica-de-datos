#include<../cabecera/c_estadistica.h>
#include<iostream>

/*
  Algoritmos para realizar Cálculos Estadisticos.
  Autor: Angel Granados

*/

using namespace std;


// Cálculo de la media.

double Media(double obs_muestra[], int len_muestra){
  
  double suma=0.0;
  for(int i=0; i < len_muestra; i++)
    suma += obs_muestra[i];
  return suma/len_muestra;
}


// Ordenar Vector.
double* VectorOrdenado(double obs_muestra[], int len_muestra){

  double caja_intercambio;
  for(int i=0; i < len_muestra; i++){
    for(int j=i+1; j < len_muestra; j++){
      if(obs_muestra[i]>obs_muestra[j]){ // Condiciones de prueba
	// Algoritmo de intercambio
	caja_intercambio = obs_muestra[j];
	obs_muestra[j] = obs_muestra[i];
	obs_muestra[i] = caja_intercambio;
      }
    }
  }

  return obs_muestra;
}



// Cálculo de la Mediana
double Mediana(double obs_muestra[], int len_muestra){

  double* med = VectorOrdenado(obs_muestra, len_muestra);
  if(len_muestra%2!=0){
    return med[len_muestra/2];
  }else{
    return (med[len_muestra/2 - 1] + med[len_muestra/2])/2.0;
  }
}

// Cálculo de la Media recortada

double MediaRec(double obs_muestra[],int len_muestra, int porcentaje){

  double* ord = VectorOrdenado(obs_muestra, len_muestra);
  int element = (porcentaje*len_muestra)/100;
  int len_muestra_nueva = len_muestra - 2*element;
  double suma = 0.0;
  
  for(int i=element; i < len_muestra - element ; i++){
    suma +=ord[i]; 
  }

  return suma/len_muestra_nueva;
  
}
