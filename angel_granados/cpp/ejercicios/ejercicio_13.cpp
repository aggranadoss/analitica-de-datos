#include<../cabecera/c_estadistica.h> 
#include<iostream>
#include<cstdlib> //para usar system

/*
  Ejercicio 1.3 Medidas de localización: La media y la mediana de una muestra (Tomado del libro de Probabilidad y Estadistica Walpole-Myers-Myers) 

  Autor del software: Angel Granados

*/


// compilador -> g++ -o ejercicios/ejercicio_13 implementacion/c_estadistica.cpp ejercicios/ejercicio_13.cpp -I cabecera/

using namespace std;

void pause(){
#ifdef _WIN32
  system("pause");
#else
  cout << "Presiona Enter para continuar ...\n" << endl;
  cin.get();
#endif
}


int main(int argc, char *argv[]){

  double muestra_11[]={3.4, 2.5, 4.8, 2.9, 3.6, 2.8, 3.3, 5.6, 3.7, 2.8, 4.4, 4.0, 5.2, 3.0, 4.8};
  int len_muestra_11 = sizeof(muestra_11)/sizeof(muestra_11[0]);
  system("cls || clear");

  cout << "*************************************************************************" << endl;
  cout << "Seccion 1.3 Medidas de localización: la media y la mediana de una muestra " << endl;
  cout << "*************************************************************************" << endl;

  cout << "\n**************" << endl;
  cout << "Ejericicio 1.1 " << endl;
  cout << "**************" << endl;
  
  cout << "\n¿Cuál es el tamaño de la muestra anterior? : " << len_muestra_11 << endl;
  cout << "Calcule la media de la muestra para estos datos : " << Media(muestra_11,len_muestra_11)  << endl;
  cout << "Calcule la mediana de la muestra : " << Mediana(muestra_11,len_muestra_11)  << endl;
  cout << "Calcule la media recortada al 20% para el conjunto de datos anterior : " << MediaRec(muestra_11,len_muestra_11,20) << endl;

  double muestra_12[]={18.71, 21.41, 20.72, 21.81, 19.29, 22.43, 20.17, 23.71, 19.44, 20.50, 18.92, 20.33, 23.00, 22.85, 19.25, 21.77, 22.11, 19.77, 18.04, 21.12};
  int len_muestra_12 = sizeof(muestra_12)/sizeof(muestra_12[0]);
  
  cout << "\n**************" << endl;
  cout << "Ejericicio 1.2 " << endl;
  cout << "**************" << endl;
  
  cout << "\n¿Cuál es el tamaño de la muestra anterior? : " << len_muestra_12 << endl;
  cout << "Calcule la media de la muestra para estos datos : " << Media(muestra_12,len_muestra_12)  << endl;
  cout << "Calcule la mediana de la muestra : " << Mediana(muestra_12,len_muestra_12)  << endl;
  cout << "Calcule la media recortada al 10% para el conjunto de datos anterior : " << MediaRec(muestra_12,len_muestra_12,10) << endl;

  cout << "\n" << endl;
  pause();
  system("cls || clear");
    
  double muestra_A13[]={227, 222, 218, 217, 225, 218, 216, 229, 228, 221 };
  double muestra_B13[]={219, 214, 215, 211, 209, 218, 203, 204, 201, 205 };
  int len_muestra_A13 = sizeof(muestra_A13)/sizeof(muestra_A13[0]);
  int len_muestra_B13 = sizeof(muestra_B13)/sizeof(muestra_B13[0]);
  
  cout << "\n**************" << endl;
  cout << "Ejericicio 1.3 " << endl;
  cout << "**************" << endl;
  
  cout << "\n¿Cuál es el tamaño de la muestra anterior del polímero sin envejecimiento? : " << len_muestra_A13 << endl;
  cout << "¿Cuál es el tamaño de la muestra anterior del polímero con envejecimiento? : " << len_muestra_B13 << endl;
  cout << "Calcule la resistencia a la tensión de la media de la muestra para el polímero sin envejecimiento  : " << Media(muestra_A13,len_muestra_A13)  << endl;
  cout << "Calcule la resistencia a la tensión de la media de la muestra para el polímero con envejecimiento  : " << Media(muestra_B13,len_muestra_B13)  << endl;
  cout << "Calcule la mediana de la muestra del polímero sin envejecimiento : " << Mediana(muestra_A13,len_muestra_A13)  << endl;
  cout << "Calcule la mediana de la muestra del polímero con envejecimiento : " << Mediana(muestra_B13,len_muestra_B13)  << endl;
  
  double muestra_A14[]={9.3, 8.8, 6.8, 8.7, 8.5, 6.7, 8.0, 6.5, 9.2, 7.0 };
  double muestra_B14[]={11.0, 9.8, 9.9, 10.2, 10.1, 9.7, 11.0, 11.1, 10.2, 9.6};
  int len_muestra_A14 = sizeof(muestra_A14)/sizeof(muestra_A14[0]);
  int len_muestra_B14 = sizeof(muestra_B14)/sizeof(muestra_B14[0]);
  
  cout << "\n**************" << endl;
  cout << "Ejericicio 1.4 " << endl;
  cout << "**************" << endl;
  
  cout << "\n¿Cuál es el tamaño de la muestra de la compañía A? : " << len_muestra_A14 << endl;
  cout << "¿Cuál es el tamaño de la muestra de la compañía B? : " << len_muestra_B14 << endl;
  cout << "Calcule la media de la muestra para la compañía A  : " << Media(muestra_A14,len_muestra_A14)  << endl;
  cout << "Calcule la media de la muestra para la compañía B  : " << Media(muestra_B14,len_muestra_B14)  << endl;
  cout << "Calcule la mediana de la muestra para la compañía A : " << Mediana(muestra_A14,len_muestra_A14)  << endl;
  cout << "Calcule la mediana de la muestra para la compañía B : " << Mediana(muestra_B14,len_muestra_B14)  << endl;

  
  cout << "\n" << endl;
  pause();
  system("cls || clear");


  double muestra_A15[]={7, 3, -4, 14, 2, 5, 22, -7, 9, 5 };
  double muestra_B15[]={-6, 5, 9, 4, 4, 12, 37, 5, 3, 3 };
  int len_muestra_A15 = sizeof(muestra_A15)/sizeof(muestra_A15[0]);
  int len_muestra_B15 = sizeof(muestra_B15)/sizeof(muestra_B15[0]);
  
  cout << "\n**************" << endl;
  cout << "Ejericicio 1.5 " << endl;
  cout << "**************" << endl;
  
  cout << "\n¿Cuál es el tamaño de la muestra del Grupo de Control? : " << len_muestra_A15 << endl;
  cout << "¿Cuál es el tamaño de la muestra del Grupo de Tratamiento? : " << len_muestra_B15 << endl;
  cout << "Calcule la media de la muestra del Grupo de Control : " << Media(muestra_A15,len_muestra_A15)  << endl;
  cout << "Calcule la media de la muestra del Grupo de Tratamiento : " << Media(muestra_B15,len_muestra_B15)  << endl;
  cout << "Calcule la mediana de la muestra del Grupo de Control : " << Mediana(muestra_A15,len_muestra_A15)  << endl;
  cout << "Calcule la mediana de la muestra del Grupo de Tratamiento : " << Mediana(muestra_B15,len_muestra_B15)  << endl;
  cout << "Calcule la media recortada al 10% del Grupo de Control : " << MediaRec(muestra_A15,len_muestra_A15,10) << endl;
  cout << "Calcule la media recortada al 10% del Grupo de Tratamiento : " << MediaRec(muestra_A15,len_muestra_A15,10) << endl;

  double muestra_A16[]={2.07, 2.14, 2.22, 2.03, 2.21, 2.03, 2.05, 2.18, 2.09, 2.14, 2.11, 2.02};
  double muestra_B16[]={2.52, 2.15, 2.49, 2.03, 2.37, 2.05, 1.99, 2.42, 2.08, 2.42, 2.29, 2.01};
  int len_muestra_A16 = sizeof(muestra_A16)/sizeof(muestra_A16[0]);
  int len_muestra_B16 = sizeof(muestra_B16)/sizeof(muestra_B16[0]);
  
  cout << "\n**************" << endl;
  cout << "Ejericicio 1.6 " << endl;
  cout << "**************" << endl;
  
  cout << "\n¿Cuál es el tamaño de la muestra de la resistencia a la tensión del caucho a 20 grados? : " << len_muestra_A16 << endl;
  cout << "¿Cuál es el tamaño de la muestra de la resistencia a la tensión del caucho a 45 grados? : " << len_muestra_B16 << endl;
  cout << "Calcule la media de la muestra de la resistencia a la tensión del caucho a 20 grados : " << Media(muestra_A16,len_muestra_A16)  << endl;
  cout << "Calcule la media de la muestra de la resistencia a la tensión del caucho a 45 grados : " << Media(muestra_B16,len_muestra_B16)  << endl;
  cout << "Calcule la mediana de la muestra de la resistencia a la tensión del caucho a 20 grados : " << Mediana(muestra_A16,len_muestra_A16)  << endl;
  cout << "Calcule la mediana de la muestra de la resistencia a la tensión del caucho a 45 grados : " << Mediana(muestra_B16,len_muestra_B16)  << endl;
  cout << "Calcule la media recortada al 10% de la resistencia a la tensión del caucho a 20 grados : " << MediaRec(muestra_A16,len_muestra_A16,10) << endl;
  cout << "Calcule la media recortada al 10% de la resistencia a la tensión del caucho a 45 grados : " << MediaRec(muestra_A16,len_muestra_A16,10) << endl;

  cout << "\n" << endl;
  pause();
  system("cls || clear");
  
  return EXIT_SUCCESS;

  
}

