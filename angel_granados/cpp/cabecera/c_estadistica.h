#ifndef C_ESTADISTICA_H
#define C_ESTADISTICA_H


// Cálculo de la media.
double Media(double obs_muestra[], int len_muestra);

// Ordenar Vector.
double* VectorOrdenado(double obs_muestra[], int len_muestra);

// Cálculo de la Mediana
double Mediana(double obs_muestra[], int len_muestra);

// Cálculo de la media recortada.
double MediaRec(double obs_muestra[],int len_muestra, int porcentaje);


#endif
